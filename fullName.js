const arrayData = require('./p2.js');

function fullName(name){
    let firstName = name.first_name;
    let lastName = name.last_name;
    name["Full_Name"] = firstName + " " + lastName; 
    return name;
}

const result = arrayData.map(fullName);

console.log(result);