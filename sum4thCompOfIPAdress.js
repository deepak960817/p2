const arrayData = require('./p2.js');
const IPAddress = require('./sum2ndCompOfIPAdress.js')

const ipArray = arrayData.map(IPAddress);

function ipSum(sumAccumulator, currentValue)
  {
     let number = parseFloat(currentValue[3]);
      sumAccumulator += number;
      return sumAccumulator;
  }

const ipSumOf4thComponent = ipArray.reduce(ipSum,0);

console.log(ipSumOf4thComponent);