const arrayData = require('./p2.js');

function IPAddress(ip){
    let string = ip.ip_address;
    return string.split('.');
}

const ipArray = arrayData.map(IPAddress);


function ipSum(sumAccumulator, currentValue)
  {
     let number = parseFloat(currentValue[1]);
      sumAccumulator += number;
      return sumAccumulator;
  }

const ipSumOf2ndComponent = ipArray.reduce(ipSum,0);

console.log(ipSumOf2ndComponent);

module.exports = IPAddress;