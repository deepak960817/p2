const arrayData = require('./p2.js');

const result = arrayData.sort(function (name1, name2) 
{
    const firstName1 = name1.first_name.toUpperCase(); 
    const firstName2 = name2.first_name.toUpperCase(); 

    if (firstName1 > firstName2) {
      return -1;
    }
    if (firstName1 < firstName2) {
      return 1;
    }
    return 0;
  });

  console.log(result);
